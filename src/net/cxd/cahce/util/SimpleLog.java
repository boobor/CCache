package net.cxd.cahce.util;

public class SimpleLog {
	public enum Lv {
		DEBUG(0), INFO(1), WARNING(2), ERROR(3);
		private int l;

		public int getL() {
			return l;
		}

		public void setL(int l) {
			this.l = l;
		}

		private Lv(int l) {
			this.l = l;
		}
	}

	private Lv lv;

	public SimpleLog(Lv lv) {
		this.lv = lv;
	}

	public void d(String tag, Object msg) {
		if (lv.l >= Lv.DEBUG.l) {
			System.out.print(tag + " ==> : " + msg);
		}
	}

	public void i(String tag, Object msg) {
		if (lv.l >= Lv.INFO.l) {
			System.out.print(tag + " ==> : " + msg);
		}
	}

	public void w(String tag, Object msg) {
		if (lv.l >= Lv.WARNING.l) {
			System.err.print(tag + " ==> : " + msg);
		}
	}

	public void e(String tag, Object msg) {
		if (lv.l >= Lv.ERROR.l) {
			System.err.print(tag + " ==> : " + msg);
		}
	}

	public void e(String tag, Exception e) {
		if (lv.l >= Lv.ERROR.l) {
			e.printStackTrace();
		}
	}
}
