package net.cxd.cahce.util;

public class Task {
	private String method;
	private String clazz;
	private Long startTime;
	private Long complateTime;
	public Task() {
		// TODO Auto-generated constructor stub
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getClazz() {
		return clazz;
	}
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	public Long getStartTime() {
		return startTime;
	}
	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}
	public Long getComplateTime() {
		return complateTime;
	}
	public void setComplateTime(Long complateTime) {
		this.complateTime = complateTime;
	}
	public Task(String method, String clazz, Long startTime, Long complateTime) {
		super();
		this.method = method;
		this.clazz = clazz;
		this.startTime = startTime;
		this.complateTime = complateTime;
	}
	
}
