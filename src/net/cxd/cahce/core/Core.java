package net.cxd.cahce.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import net.cxd.cahce.test.Test;

public class Core {
	private static final Map<Object, Object> db = new HashMap<Object, Object>();
	private static Core core;
	public Connection getConnection(Class clazz){
		String key = clazz.getName();
		Map<Object, Object> map = (Map<Object, Object>) db.get(key);// 获得表缓存对象，存储进去
		if (map == null) {
			map = new ConcurrentHashMap<Object, Object>();
		}
		Map<Object, Object> keyMap = (Map<Object, Object>) db.get(key
				+ "_key");
		if (keyMap == null) {
			keyMap = new ConcurrentHashMap<Object, Object>();
		}
		Connection connection = new Connection(map,clazz,keyMap);
		return connection;
	}
	
	private  Core() {
	}
	public static Core getCore(){
		if (core == null) {
			core = new Core();
		}
		return core;
	}
	
	
	public static void main(String[] args) {
		Connection conn = new Core().getConnection(Test.class);
		long t1 = System.currentTimeMillis();
		for (int i = 0; i < 1000; i++) {
			Test o = new Test();
			o.setId(i);
			o.setName("张三" + i);
			conn.save(o);
		}
		long t2 = System.currentTimeMillis();
		System.out.println((t2-t1 )+ "   ms");
		Test test1 = new Test();
		test1.setId(503);
		Object ooo = conn.find(test1);
		long t3 = System.currentTimeMillis();
		System.out.println(t3-t2 + "   ms");
		System.out.println(ooo);
		
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", "张三" + 505);
		List<Object> list = conn.findByKeys(map);
		System.out.println(System.currentTimeMillis() - t3 +"   ms");
		
		System.out.println(list.size());
		
	}
	
	
	
	
	
	
	
	/**
	private  DBExeListener dbListener;
	private SimpleLog log ;
	
	public Core() {
		log = new SimpleLog(SimpleLog.Lv.DEBUG);
		dbListener = new SimpleListener ();
	}
	public void save(Object o, DBExeListener listener) {
		if (listener == null) {
			listener = dbListener ;
		}
		listener.onStart();
		try {
			String key = o.getClass().getName();
			Map<Object, Object> map = (Map<Object, Object>) db.get(key);// 获得表缓存对象，存储进去
			if (map == null) {
				map = new HashMap<Object, Object>();
				db.put(key, map);
			}
			Map<Object, Object> keyMap = (Map<Object, Object>) db.get(key
					+ "_key");
			if (keyMap == null) {
				keyMap = new HashMap<Object, Object>();
				db.put(key + "_key", keyMap);
			}
			Field[] fs = o.getClass().getDeclaredFields();
			Object key1 = null;
			for (int i = 0; i < fs.length; i++) {
				if (fs[i].getAnnotation(Id.class) != null) {
					fs[i].setAccessible(true);
					key1 = fs[i].get(o);
					map.put(key1, o);// key --> value
				} else if (fs[i].getAnnotation(Key.class) != null) {
					// TODO save the other key
					fs[i].setAccessible(true);
					List<Object> list = (List<Object>) keyMap.get(fs[i].get(o));
					if (list == null) {
						list = new ArrayList<Object>();
					}

					if (key1 != null) {
						list.add(key1);
					} else {
						for (int j = 0; i < fs.length; j++) {
							if (fs[j].getAnnotation(Id.class) != null) {
								fs[j].setAccessible(true);
								key1 = fs[j].get(o);
								list.add(key1);
								// map.put(key1, o);// key --> value
							}
						}
					}

					keyMap.put(fs[i].get(o), list);
				}
			}
			listener.onComplate();
		} catch (Exception e) {
			listener.onError(e);
		}
	}

	public void remove(Object o) {
		try {
			String key = o.getClass().getName();
			Map<Object, Object> map = (Map<Object, Object>) db.get(key);// 获得表缓存对象，存储进去
			if (map == null) {
				map = new HashMap<Object, Object>();
				db.put(key, map);
				return;
			}
			Field[] fs = o.getClass().getDeclaredFields();
			Object key1 = null;
			for (int i = 0; i < fs.length; i++) {
				if (fs[i].getAnnotation(Id.class) != null) {
					fs[i].setAccessible(true);
					key1 = fs[i].get(o);
					break;
				}
			}

			Map<Object, Object> keyMap = (Map<Object, Object>) db.get(key
					+ "_key");
			if (keyMap == null) {
				keyMap = new HashMap<Object, Object>();
				db.put(key + "_key", keyMap);
				return;
			}

			for (int i = 0; i < fs.length; i++) {
				if (fs[i].getAnnotation(Key.class) != null) {
					fs[i].setAccessible(true);
					Object key3 = fs[i].get(o);
					List<Object> list = (List<Object>) keyMap.get(key3);
					if (list == null) {
						continue ;
					}else{
						for (Object obj : list) {
							if (obj == key1) {
								list.remove(obj);
								keyMap.put(key3, list);
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void find(Object o){
	}
	
	
	
	public interface DBExeListener{
		void onStart();
		void onComplate();
		void onError(Exception e);
	}
	*/
	
}
