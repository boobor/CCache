package net.cxd.cahce.test;

import net.cxd.cahce.ann.Id;
import net.cxd.cahce.ann.Key;
public class Test {
	@Id
	private int id;
	@Key(face=1)
	private String name;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
